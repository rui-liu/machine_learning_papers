## 训练方法
- [Population Based Training of Neural Networks](pdf/1711.09846.pdf)
- [Online Learning: Theory, Algorithms, and Applications](pdf/ShalevThesis07.pdf)

## IR
- [Learning Deep Structured Semantic Models for Web Search using Clickthrough Data](pdf/cikm2013_DSSM_fullversion.pdf)

微软的深度学习相似模型

## Recommendation
- [Attentive Collaborative Filtering: Multimedia Recommendation with Item- and Component-Level Aention](pdf/SIGIR17_attention.pdf)
